public class Assistant {
    private Storage storage;

    public Assistant(Storage storage) {
        this.storage = storage;
    }

    public Ticket save(Luggage luggage) {
        return this.storage.save(luggage);
    }

    public Luggage getLuggage(Ticket ticket) {
        return this.storage.get(ticket);
    }
}
