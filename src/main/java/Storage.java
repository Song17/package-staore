import java.util.HashMap;

public class Storage {
    private int size;
    private HashMap<Ticket, Luggage> luggageMap = new HashMap<>();
    private HashMap<Ticket, Slot> slots = new HashMap<>();

    public Storage() {
        this(10);
    }

    public Storage(int size) {
        this.size = size;
    }

    public Ticket save(Luggage luggage) {
        if (this.luggageMap.size() < this.size) {
            Ticket ticket = new Ticket();
            luggageMap.put(ticket, luggage);
            return ticket;
        } else {
            throw new RuntimeException("Out of capacity");
        }
    }

    public Luggage get(Ticket ticket) {
        return luggageMap.remove(ticket);
    }

    public Ticket save(Luggage small, int slotId) {
        Slot slot = new Slot();
        slot.save(small);
        Ticket ticket = new Ticket();
        slots.put(ticket, slot);
        return ticket;
    }
}
