public class Luggage {
    private int size;

    public Luggage(int size) {
        this.size = size;
    }

    public Luggage() {
    }

    public int getSize() {
        return size;
    }
}
