import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class LuggageStoreTest {
    @Test
    public void should_save_luggage() {
        Luggage luggage = new Luggage();
        Storage storage = new Storage();
        Ticket ticket = storage.save(luggage);

        assertNotNull(ticket);
    }

    @Test
    public void should_get_luggage() {
        Luggage luggage = new Luggage();
        Storage storage = new Storage();

        Ticket ticket = storage.save(luggage);
        Luggage savedLuggage = storage.get(ticket);

        assertEquals(luggage, savedLuggage);
    }

    @Test
    public void should_get_second_luggage() {
        Storage storage = new Storage();
        Luggage luggage = new Luggage();
        Luggage secondLuggage = new Luggage();

        storage.save(luggage);
        Ticket secondTicket = storage.save(secondLuggage);

        assertEquals(secondLuggage, storage.get(secondTicket));
    }

    @Test
    public void should_get_null_when_save_null() {
        Storage storage = new Storage();
        Ticket ticket = storage.save(null);

        assertNull(storage.get(ticket));
    }

    @Test
    public void should_get_null_when_use_a_same_ticket_twice() {
        Storage storage = new Storage();
        Luggage luggage = new Luggage();

        Ticket ticket = storage.save(luggage);

        Luggage firstGottenLuggage = storage.get(ticket);
        assertEquals(luggage, firstGottenLuggage);

        Luggage secondGottenLuggage = storage.get(ticket);
        assertNull(secondGottenLuggage);
    }

    @Test
    public void should_throw_exception_if_storage_is_full() {
        Storage storage = new Storage(1);
        storage.save(new Luggage());

        RuntimeException thrown = assertThrows(RuntimeException.class, () -> storage.save(new Luggage()));
        assertEquals("Out of capacity", thrown.getMessage());
    }

    @Test
    void should_store_small_luggage_in_small_slot() {
        Luggage small = new Luggage(1);
        Storage storage = new Storage();
        Ticket ticket = storage.save(small, 1);

        assertNotNull(ticket);
    }
}
