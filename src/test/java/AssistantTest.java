import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AssistantTest {
    private Storage mockStorage = mock(Storage.class);

    @Test
    public void should_get_ticket_when_save_luggage() {
        Assistant assistant = new Assistant(mockStorage);
        Luggage luggage = new Luggage();
        Ticket ticket = new Ticket();
        when(mockStorage.save(luggage)).thenReturn(ticket);

        Ticket result = assistant.save(luggage);

        assertEquals(ticket, result);
    }

    @Test
    public void should_get_luggage() {
        Assistant assistant = new Assistant(mockStorage);
        Ticket ticket = new Ticket();
        Luggage luggage = new Luggage();
        when(mockStorage.get(ticket)).thenReturn(luggage);

        Luggage result = assistant.getLuggage(ticket);

        assertEquals(luggage, result);
    }

    @Test
    public void should_get_null_when_save_null() {
        Assistant assistant = new Assistant(mockStorage);
        Ticket ticket = new Ticket();

        when(mockStorage.save(null)).thenReturn(ticket).getMock();
        when(mockStorage.get(ticket)).thenReturn(null);

        Ticket ticket1 = assistant.save(null);
        assertNull(assistant.getLuggage(ticket1));
    }

    @Test
    public void should_get_null_when_use_a_ticket_twice() {
        Assistant assistant = new Assistant(mockStorage);
        Ticket ticket = new Ticket();

        when(mockStorage.get(ticket)).thenReturn(new Luggage()).thenReturn(null);

        Luggage luggage = assistant.getLuggage(ticket);
        assertNotNull(luggage);
        Luggage secondGottenLuggage = assistant.getLuggage(ticket);

        assertNull(secondGottenLuggage);
    }

    @Test
    public void should_throw_exception_when_out_of_capacity() {
        Assistant assistant = new Assistant(mockStorage);

        when(mockStorage.save(any(Luggage.class)))
                .thenReturn(new Ticket())
                .thenReturn(new Ticket())
                .thenReturn(new Ticket())
                .thenThrow(new RuntimeException("Out of capacity"));

        for (int i = 0; i < 3; i++) {
            assistant.save(new Luggage());
        }

        RuntimeException thrown = assertThrows(RuntimeException.class, () -> assistant.save(new Luggage()));

        assertEquals("Out of capacity", thrown.getMessage());
        verify((mockStorage), times(4)).save(any(Luggage.class));
    }
}
